import java.util.Iterator;

public class Lista<E> implements IList<E>, Iterable {

	Element<E> head;
	
	
	@Override
	public boolean isEmpty() {
		if(head==null) return true;
		
		else return false;
	}

	@Override
	public void clear() {
		head=null;
		
		
	}

	@Override
	public int size() {
		int i=1;
		if(head!=null) {
			Element<E> counter=head;
			while(counter.next!=null) {
				counter=counter.next;
				i++;
				}
			return i;
		}
		else return 0;
	}

	@SuppressWarnings("unchecked")
	@Override
	public boolean add(E value) {
		if(this.size()==0) {
			head=new Element<E>(value);
			return true;
		}
		else {
		Element<E> counter=head;
		while(counter.next!=null) {
			counter=counter.next;
		}
	//	System.out.println(counter.value+ "  " + counter.next);
		counter.next=new Element(value);
		return true;
	}
	}

	@Override
	public boolean add(int index, E value) {
		Element<E> counter=head;	
		Element<E> temporary=new Element<E>(value);
		if(index==0) {
			head=new Element<E>(value);
			head.next=counter;
			return true;
		}
		else if(index==1) {
			temporary.next=head.next;
			head.next=temporary;
			return true;
		}
		else if(index>1 && index<this.size()) {
		for(int i=0;i<index-1;i++) 
		counter=counter.next;
		
		temporary.next=counter.next;
		counter.next=temporary;
		return true;
		}
		
		return false;
	}

	@Override
	public boolean contains(E value) {
		Element<E> counter=head;
		while(counter.next!=null) {
			if(counter.value==value) return true;
			
			counter=counter.next;			
		}
		return false;
	}

	@Override
	public E get(int index) {
		Element<E> counter=head;

		for(int i=0;i<index;i++) {
			counter=counter.next;
		}
		return counter.value;
	}

	@Override
	public E set(int index, E value) {
		Element<E> counter=head;
		if(index>=0 && index<this.size()) {
		for(int i=0;i<index;i++) {
			counter=counter.next;
		}
		counter.value=value;
		return counter.value;
		}
		else return null;
	}

	@Override
	public int indexOf(E value) {
		Element<E> counter=head;
		int i=0;
		while(counter.next!=null) {
			if(counter.value==value) {
				return i;
			}
		counter=counter.next;
		i++;
		}
		
		return -1;
	}

	@Override
	public E remove(int index) {
		Element<E> counter=head;
		
		if(index==0) {
			head=head.next;
			return head.value;
		}
		else if(index>0 && index<this.size()) {
			for(int i=1;i<index;i++) 
				counter=counter.next;
			
			counter.next=counter.next.next;
			return (E) counter.next.value;
		}
		
		return null;
	}

	@Override
	public boolean remove(E value) {
		int i=this.indexOf(value);
		if(i==-1)
			return false;
		else {
			this.remove(i);
		return true;
		}
	}

	@Override
	public Iterator iterator() {
		// TODO Auto-generated method stub
		return null;
	}

}
