public interface IList <E> {
	
boolean isEmpty(); // zwraca true, je�li lista jest pusta (head==null)

void clear(); // czy�ci list�

int size(); // zwraca liczb� element�w w li�cie

boolean add (E value); // dodanie do listy, zwraca true, je�li element dodany

boolean add(int index, E value);// dodanie na okre�lon� pozycj�

boolean contains (E value); //czy lista zawiera podan� warto��

E get (int index); //pobiera warto�� spod podanej pozycji

E set (int index, E value); //ustawia now� warto�� na podanej pozycji

int indexOf (E value); //zwraca pozycj� podanej warto�ci lub -1

E remove (int index); //usuwa element z podanej pozycji, zwraca jego warto��

boolean remove (E value); // usuwa warto��

}