
public class Klient {
	
	Queue<Produkt> kolejka = new Queue<>();
	String nazwaKlienta;
	
	
	Klient(String nazwa)
	{
		nazwaKlienta=nazwa;
	}
	
	void dodajProdukt(Produkt produkt)
	{
		try 
		{
			kolejka.enqueue(produkt);
		}
		catch(Exception e)
		{
			System.out.println("ERROR");
		}
	}
	
	int podsumuj()
	{
		try 
		{
			int buf = 0;
			while(!kolejka.isEmpty())
			{
				Produkt produkt = kolejka.dequeue();
				buf = buf +produkt.cena*produkt.iloscProduktu;
			}
			return buf;
		}
		catch(Exception e)
		{
			System.out.println("ERROR");
			return -1;
		}
	}
}
