
public class Magazyn {
	Queue<Klient> kolejka = new Queue<>();
	int zarobekMagazynu=0;
	
	void dodajKlienta(Klient klient)
	{
		try 
		{
			kolejka.enqueue(klient);
		}
		catch(Exception e)
		{
			System.out.println("ERROR");
		}	
	}
	
	int getZarobekMagazynu()
	{
		int buf=0;
		try 
		{	
			while(!kolejka.isEmpty())
			{
					Klient klient = kolejka.dequeue();
					int actual= klient.podsumuj();
					System.out.print("Klient: " + klient.nazwaKlienta + " Suma: " + actual+"\n");
					buf = buf + actual;
			}
		}
		catch(Exception e)
		{
			System.out.println("ERROR");
			return -1;
		}
		return buf;
	}
	
	/*int podsumujKlientow()
	{
		int buf=0;
		try 
		{	
			while(!kolejka.isEmpty())
			{
					Klient klient = kolejka.dequeue();
					buf = buf + klient.podsumuj();
			}
		}
		catch(Exception e)
		{
			System.out.println("ERROR");
			return -1;
		}
		return buf;
	}*/
	
}
