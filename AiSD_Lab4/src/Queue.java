
public class Queue<T> implements IQueue<T>{
	
	Element<T> head;
	
	
	@Override
	public boolean isEmpty() {
		if(head==null)
			return true;
		else 
			return false;
	}

	@Override
	public boolean isFull() {
		return false;
	}

	@Override
	public T dequeue() throws EmptyQueueException {
		try
		{
			T buf=head.value;
			head=head.next;
			return buf;
		}
		catch(Exception e)
		{
			throw new EmptyQueueException();
		}
	}

	@Override
	public void enqueue(T elem) throws FullQueueException {
	
		if(head!=null)
		{
			Element<T> current = head;
			while(current.next!=null)
			{
				current=current.next;
			}
			current.next=new Element<T>();
			current.next.value=elem;
		}
		else
		{
			head = new Element<T>();
			head.value = elem;
		}
	}

	@Override
	public int size() {
		if(head==null) return 0;
		int i=1;
		Element<T> current = head;
		while(current.next != null)
		{
			++i;
			current=current.next;
		}
		return i;
	}

	@Override
	public T first() throws EmptyQueueException {
		try
		{
			return head.value;
		}
		catch(Exception e)
		{
			throw new EmptyQueueException();
		}
	}

	
}
