
public class Firma {
	Queue<Magazyn> magazyny = new Queue<>();
	
	void dodajMagazyn(Magazyn n)
	{
		try
		{
		magazyny.enqueue(n);
		}
		catch(Exception e)
		{
			System.out.println("ERROR");
		}
	}
	
	int podsumujMagazyn()
	{
		try
		{
		return magazyny.first().getZarobekMagazynu();
		}
		catch(Exception e)
		{
			System.out.println("ERROR");
			return -1;
		}
	}
	
	int zarobekFirmy()
	{
		try 
		{
			int iter=0;
			int buf = 0;
			while(!magazyny.isEmpty())
			{
				iter++;
				Magazyn magazyn = magazyny.dequeue();
				//System.out.println(magazyn.getZarobekMagazynu());
				int aktualnyMagazyn=magazyn.getZarobekMagazynu();
				buf = buf + aktualnyMagazyn;
				System.out.println("Magazyn "+iter + ": " + aktualnyMagazyn);
				System.out.println();
			}
			return buf;
		}
		catch(Exception e)
		{
			System.out.println("ERROR");
			return -1;
		}
	}
}
