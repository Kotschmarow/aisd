
public interface Stack <E>  {
	
	boolean empty();		//  zwraca true, jeli stos jest pusty,
	//E push(E element); 		// umieszcza znak na szczycie stosu i zwraca referencje do niego,
	E pop(); 				// zwraca znak ze szczytu stosu i usuwa go ze stosu,
	E peek();				//  zwraca znak ze szczytu stosu bez jego zdejmowania.
	E push(E element);
}
